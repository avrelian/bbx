module.exports = function(grunt) {

  grunt.initConfig({

    coffee: {
      compile: {
        options: {
          join: true
        },
        files: [
          {
            'index.js': ['extensions/**/*.coffee']
          }
        ]
      }
    },

    watch: {
      coffee: {
        files: ['extensions/**/*.coffee'],
        tasks: ['coffee']
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-shell');

  grunt.registerTask('default', ['coffee', 'watch']);
};
