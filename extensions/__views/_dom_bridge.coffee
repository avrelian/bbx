###
  прототип отображения для работы с DOM
###

class Extensions.View.DomBridge extends Extensions.View.RouterBridge

  removeTimeout: 0.2 * 60 * 1000

  initialize: ->
    super

    if @options.template
      @template = @options.template

    if not @options.dontRenderOnInit
      @render()

  keepPlaceholder: ->
    if @options.el
      @$placeholderEl ||= @$el

  restorePlaceholder: ->
    if @$placeholderEl
      @$el.after(@$placeholderEl)

  render: ->
    @renderHTML()
    @onRender()
    @

  onRender: ->
    @selectElements()

  serializeData: ->
    _.extend(@getPath(), @getState(), @model?.toJSON())

  getState: ->
    {}

  # TODO: отрефакторить - avrelian 2014.04.03
  getPath: ->
    if @callSign
      path = '#' + router.routePrefix + @callSign.slice('show/'.length)
      if path[path.length - 1] == '/'
        path = path.slice(0, path.length - 1)

    path: path
    currentPath: '#' + router.currentRoute

  renderHTML: ->
    if @template
      @keepPlaceholder()

      outerEl = $(@template(@serializeData()))

      # Потеря родительским отображением ссылки на элемент - ожидаемое поведение.
      # Если родительское отображение захочет передать placeholder-элемент в другое отображение, оно сначала должно удалить текущее.
      @$el.replaceWith(outerEl)
      @setElement(outerEl)

  selectElements: ->
    if @elements
      @_elements_selectors ||= @elements
      @_additionalPartialData = {}
      @elements = {}
      for key, value of @_elements_selectors
        selector = value.el ? value

        @selectElement(key, selector)

        @_additionalPartialData[key] = value.data or ->

  selectElement: (key, selector) ->
    @elements[key] = @$(selector)

  onRouterNotification: (routeTail) =>
    super

    @show(routeTail)

  show: (routeTail) ->
    @onShow()
    @$el.show()

  onShow: ->
    if @removeSchedule
      clearTimeout(@removeSchedule)
      @removeSchedule = null

  hide: ->
    @onHide()
    @$el.hide()

  onHide: ->
    if @removeTimeout
      @removeSchedule = setTimeout( =>
        @remove()
      @removeTimeout)

  remove: ->
    @onRemove()
    super

  onRemove: ->
    if @removeSchedule
      clearTimeout(@removeSchedule)

    @trigger('removing')
    @restorePlaceholder()