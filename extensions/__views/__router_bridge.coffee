###
  прототип отображения для работы с рутером
###

class Extensions.View.RouterBridge extends Backbone.View

  @extensionOf: (constructor) ->
    Extensions.utils.extensionOf(constructor, @)

  callSign: false

  initialize: (options) ->
    super

    @options = options or {}

    @listenToRouter()

  listenToRouter: ->
    if @options.callSign
      @callSign = @options.callSign

    if @callSign
      @listenTo(router, @callSign, @onRouterNotification)

  onRouterNotification: =>
    router.acknowledgeReceipt(@)