###
  прототип коммутатора и селектора отображений
###

class Extensions.Commutator extends Extensions.Container

  show: (routeTail) ->
    super

    key = routeTail[0]
    if @exists(key)
      if not @isCurrent(key)
        @hideView(@currentKey)
        @currentKey = key
      @showView(key, routeTail.slice(1))
    else
      @hideView(@currentKey)

  hide: ->
    @hideView(@currentKey)
    @currentKey = null

    super

  exists: (key) ->
    !!@collection.get(key)

  clear: (key) ->
    @stopListening(@views[key])
    if @isCurrent(key)
      @_currentView = null
    @views[key] = null

  isCurrent: (key) ->
    @currentKey and @currentKey == key