###
  прототип списка из элементов коллекции
###

class Extensions.List extends Extensions.Container

  maxSelected: 0

  initialize: ->
    super

    if @collection
      @listenTo(@collection, 'add', (model) =>
        @showView(model.id)
      )

    @initSelections()

  show: (routeTail) ->
    super

    if @collection
      @collection.each( (model) =>
        @showView(model.id, routeTail)
      )

  hide: ->
    for key of @views
      @hideView(key)

    super

  clear: (key) ->
    @stopListening(@views[key])
    @views[key] = null

  initSelections: ->
    if @maxSelected and @collection
      @selectedItems = new Extensions.Collection

      @collection.on('select', @onSelect)

  onSelect: (model) =>
    if @selectedItems.get(model.id)
      @selectedItems.remove(model)
      model.trigger('unselected')
    else
      @selectedItems.add(model)

      if @selectedItems.length > @maxSelected
        removingModel = @selectedItems.first()
        @selectedItems.remove(removingModel)
        removingModel.trigger('unselected')

      model.trigger('selected')

