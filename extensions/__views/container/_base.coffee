###
  прототип отображения-контейнера
###

class Extensions.Container extends Extensions.View.DomBridge

  viewKey: (model) ->
    model.id

  initialize: ->
    super

    if @options.viewKey
      @viewKey = @options.viewKey

    if @options.view
      @view = @options.view

    @view ||= {}

    @views = {}

  show: (routeTail) ->
    @$el.show()

  hide: ->
    @$el.hide()

  ensureView: (key) ->
    # если отображение уже было создано, то использовать его (иначе - создать новое)
    if not @views[key]
      @views[key] = @createView(key)

      if @views[key]

        # при создании отображения ему уже могла быть передана ссылка на элемент-плэйсхолдер
        if not @$el.has(@views[key].$el).length
          @$el.append(@views[key].$el)

        @listenTo(@views[key], 'removing', => @clear(key))

  createView: (key) ->
    model = @collection.get(key)

    if model
      view = @view[@viewKey(model)]

    if view instanceof Function
      view = view.call(@, model)

    if view?[0]?.extensionOf(Extensions.View.DomBridge)
      new (view[0])(_.extend(
        model: model
        callSign: if @ instanceof Extensions.Commutator then "#{@callSign}/#{key}" else @callSign
        parentView: @ # бывший list - для работы выделений
      view[1]))

  showView: (key, routeTail) ->
    @ensureView(key)

    if @views[key]
      # TODO: не передавать весь routeTail, если у дочернего вида он короче - avrelian 2014.04.19
      @views[key].show(routeTail)

  # TODO: удалять по таймауту скрываемое отображение - avrelian 2013.09.12
  hideView: (key) ->
    if @views[key]
      @views[key].hide()

  clear: (key) ->
    @stopListening(@views[key])
    @views[key] = null

  onRemove: ->
    for view of @views
      view?.remove()
    super
