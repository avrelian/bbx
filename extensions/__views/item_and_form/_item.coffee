###
  прототип отображения модели
###

class Extensions.View.Item extends Extensions.View.DomBridge

  initialize: ->
    super

    @initModelEvents()

  getState: ->
    _.extend(super, @getSelectionState())

  getSelectionState: ->
    selected: !!@options.parentView?.selectedItems?.get(@model?.id)

  initModelEvents: ->
    if @model

      @modelEvents ||= {}

      @modelEvents = Extensions.utils.flattenTree(@modelEvents)

      @initAutoRender()

      for event, handlers of @modelEvents
        do (event, handlers) =>
          if _.isString(handlers) or _.isFunction(handlers)
            handlers = [handlers]

          for handler in handlers
            do (handler) =>
              if _.isString(handler) and _.isFunction(@[handler])
                handler = @[handler]

              if _.isFunction(handler)
                @listenTo(@model, event.replace(':_', ''), handler)

  # обновление отображения при наступлении различных событий модели
  #  autoRender:
  #  # рендерит все
  #    _all: 'change'
  #
  #  # обновляет корневой элемент
  #    _root: 'select'
  #
  #  # частичный рендеринг
  #    asdasd:
  #      change:
  #        asd: ''
  #        wer: ''
  #    wewer: 'werwer'

  initAutoRender: ->
    if @autoRender

      # запись
      #   autoRender: events
      # эквивалентна записи
      #   autoRender:
      #     _all: events
      if _.isString(@autoRender) or _.isArray(@autoRender)
        @autoRender = _all: @autoRender

      for renderType, eventsTree of @autoRender
        do (renderType, eventsTree) =>

          getRenderMethod = =>
            switch renderType
              when '_all' then @render
              when '_root' then @partiallyRenderRoot
              else (key, branch, path) =>
                @partiallyRender(renderType)

          events = Extensions.utils.flattenTree(eventsTree, getContent: getRenderMethod)

          for event, handler of events
            if not @modelEvents[event]
              @modelEvents[event] = []

            if not _.isArray(@modelEvents[event])
              @modelEvents[event] = [@modelEvents[event]]

            @modelEvents[event].push(handler)

  partiallyRender: (elementKey) =>
    data = @serializeData()
    data['_render_partial'] = elementKey
    partial_el = $(@template(data))
    @elements[elementKey].replaceWith(partial_el)
    @elements[elementKey] = partial_el

  partiallyRenderRoot: =>
    data = @serializeData()
    data['_render_root'] = true
    @updateRootAttributes($(@template(data)))

  updateRootAttributes: (outerEl) ->
    for attribute in outerEl.prop('attributes')
      @$el.attr(attribute.name, attribute.value)