###
  прототип формы для редактирования данных модели
###

class Extensions.View.Form extends Extensions.View.Item

  initialize: ->
    @originalModel = @model
    @model = @originalModel.clone()

    # TODO: попытаться генерализовать или обойтись без этого - avrelian 2013.11.08
    @model.callSign = @options?.callSign.replace('/edit', '')

    super

  onRender: ->
    super

    @bindValues()

  getState: ->
    _.extend(super, @getFormState())

  getFormState: ->
    isVisible = !!@isFormVisible
    hasChanged = !_.isEqual(@originalModel.attributes, @model.attributes)

    data = {}
    data['cancelable'] = isVisible
    data['savable'] = hasChanged and isVisible
    data['continuable'] = hasChanged and not isVisible
    data

  bindValues: ->
    if @values
      for attrName, value of @values
        @bindValue(attrName, value)

  bindValue: (attrName, value) ->
    selector = value.el ? value
    el = @$(selector)
    events = value.events ? 'change keydown'
    process = value.process ? (val) -> val

    if el.is('input[type=text], textarea')
      @$el.on(events, selector, (e) =>
        if e.type == 'keydown'
          switch e.keyCode
            when Extensions.utils.keyCode.ESCAPE
              oldValue = @originalModel.get(attrName)
              @model.set(attrName, oldValue)
              el.val(oldValue)
            else
              setTimeout( =>
                @model.set(attrName, process(el.val()))
              50)
      )

    # TODO: вынести url в настройки конкретной формы - avrelian 2013.09.14
    if el.is('input[type=file]')
      el.fileupload(
        url: 'api/brand/logo'
        dataType: 'json'
        add: (e, data) =>
          Extensions.utils.readImage(data.files[0], (fileReader) =>
            if fileReader
              @model.set(attrName, fileReader.target.result)
          )
          data.submit()
        done: (e, data) =>
          @model.set(attrName, data.result.files[0].url)
      )

  saveForm: (onSuccess) =>
    @saveModel(onSuccess)
    @remove()

  cancelForm: =>
    @remove()

  onShow: ->
    super

    @isFormVisible = true
    @model.trigger("change")

  onHide: ->
    super

    @isFormVisible = false
    @model.trigger("change")

  onRemove: ->
    super

    @trigger('delete')

  saveModel: (onSuccess) ->
    # TODO: сделать так, чтобы вложенные модели не пересоздавались - avrelian 2013.08.21
    @originalModel.set(@model.attributes)
    @originalModel.save(success: onSuccess)
    @originalModel.trigger('change')