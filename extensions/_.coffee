window.Extensions =

  View: {}

  utils:

    keyCode:
      BACKSPACE: 8,
      COMMA: 188,
      DELETE: 46,
      DOWN: 40,
      END: 35,
      ENTER: 13,
      ESCAPE: 27,
      HOME: 36,
      LEFT: 37,
      NUMPAD_ADD: 107,
      NUMPAD_DECIMAL: 110,
      NUMPAD_DIVIDE: 111,
      NUMPAD_ENTER: 108,
      NUMPAD_MULTIPLY: 106,
      NUMPAD_SUBTRACT: 109,
      PAGE_DOWN: 34,
      PAGE_UP: 33,
      PERIOD: 190,
      RIGHT: 39,
      SPACE: 32,
      TAB: 9,
      UP: 38

    # Вложенные модели/коллекции должны сериализоваться полностью
    nestedToJSON: (data) ->
      for key, value of data
        if value instanceof Extensions.Model or value instanceof Extensions.Collection
          data[key] = value.toJSON()

      data

    readImage: (file, callback, method) ->
      if window.FileReader
        fileReader = new FileReader()
        fileReader.onload = fileReader.onerror = callback
        method = method || 'readAsDataURL'
        if fileReader[method]
          fileReader[method](file)
          return fileReader
      false

    # Превращает дерево вида
    #   root:
    #     branch_1:
    #       branch_1_1: 'leaf_1_1'
    #       branch_1_2: 'leaf_1_2'
    #     branch_2:
    #       branch_2_1: ['leaf_2_1', 'leaf_2_2']
    # в плоский объект вида
    #   'root:branch_1:branch_1_1': 'leaf_1_1'
    #   'root:branch_1:branch_1_2': 'leaf_1_2'
    #   'root:branch_2:branch_2_1': ['leaf_2_1', 'leaf_2_2']
    flattenTree: (tree, options={}) ->
      flattenedTree = {}

      _.defaults(options,
        glue: ':'
        # определяет, когда закончился путь к контенту и начался собственно контент
        shouldStop: (key, branch) ->
          not _.isObject(branch) or _.isArray(branch) or _.isFunction(branch)
        # определяет в каком виде будет сохранен контент
        getContent: (key, branch, path) ->
          branch
      )

      if _.isObject(tree)
        do flatten = (tree, path=[]) ->
          for key, branch of tree
            do (key, branch, path=path.slice(0)) ->
              path.push(key)
              if options.shouldStop(key, branch)
                flattenedTree[path.join(options.glue)] = options.getContent(key, branch, path)
              else
                flatten(branch, path)
      else
        flattenedTree[tree] = options.getContent(tree)

      flattenedTree

    extensionOf: (baseConstructor, extendedConstructor) ->
      if not extendedConstructor
        return false

      if extendedConstructor.__super__?.constructor == baseConstructor
        return true

      return Extensions.utils.extensionOf(baseConstructor, extendedConstructor.__super__?.constructor)
