###
  прототип набора разнотипных данных

  new Extensions.Set(
    ship: shipModel
    planes: planesCollection
  )
###

class Extensions.Set extends Extensions.Collection

  constructor: (data, options) ->
    models = []
    if data
      for dataKey, dataPiece of data
        models.push(new Extensions.Model(
          id: dataKey
          data: dataPiece
        ))

    super(models, options)