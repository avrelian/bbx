###
  прототип коллекции
###

class Extensions.Collection extends Backbone.Collection

  model: Extensions.Model

  initialize: ->
    super

    @createNewModel()

  toJSON: ->
    Extensions.utils.nestedToJSON(super())

  createNewModel: ->
    @newModel = new @model
    @newModel.collection = @
    @newModel.id = 'new'

    @listenTo(@newModel, 'change', =>
      @trigger('change:newModel')
    )

  trigger: (name) ->
    super

    if name.indexOf('change') == 0
      @trigger('deep:change', name.split(':').slice(1).join(':'))