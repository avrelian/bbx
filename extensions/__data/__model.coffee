###
  прототип модели
###

class Extensions.Model extends Backbone.Model

  toJSON: (options) ->
    data = Extensions.utils.nestedToJSON(super())

    if @ is @collection?.newModel or (@originalModel and @originalModel is @originalModel?.collection?.newModel)
      data.id = 'new'

    data

  set: (key, val, options) ->
    if typeof key is 'object'
      attributes = key
      options = val
    else
      attributes = {}
      attributes[key] = val

    attributes = @restoreByKeys(attributes)

    super(attributes, options)

    # генерация события change у текущей модели при изменении данных у модели-атрибута
    for attr, value of @changed
      do (attr, value) =>
        if value instanceof Extensions.Model or value instanceof Extensions.Collection
          @listenTo(value, 'deep:change', =>
            @trigger("change:#{attr}")
            @trigger('change')
          )

    @

  unset: (attr) ->
    value = @get(attr)
    if value instanceof Extensions.Model or value instanceof Extensions.Collection
      @stopListening(value, 'deep:change')

    super

  # Воссоздание данных модели по ключам
  restoreByKeys: (attributes) ->
    for attr, source of @keys
      for key, sourceObj of source
        if id = attributes[key]
          if data = sourceObj().get(attributes[key])
            attributes[attr] = data
            delete attributes[key]

    attributes

  save: (options) ->
    if @id == 'new'
      # чтобы не потерять связь отображений с текущей моделью, создадим и добавим в коллекцию другую модель
      newModel = @clone()
      newModel.save(options)
      @collection.add(newModel.set('id', _.uniqueId('')))

      # очистим данные текущей модели
      @set(new @constructor().toJSON())
    else
      super(@attributes, options)

  trigger: (name) ->
    super

    if name is 'change'
      @trigger('deep:change')

  isEmpty: ->
    if _.isEmpty(@attributes)
      return true

    defaults = _.result(@, 'defaults')
    for attribute, value of @attributes
      isNewPropAdded = -> not attribute in defaults
      isDefaultValueChanged = -> not _.isObject(value) and defaults[attribute] != value
      isObjectEmpty = -> _.isObject(value) and value.isEmpty and not value.isEmpty()

      if isNewPropAdded() or isDefaultValueChanged() or isObjectEmpty()
        return false

    return true