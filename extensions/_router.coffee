###
  прототип рутера
###

class Extensions.Router extends Backbone.Router

  # служит для отсечения лишних или неправильных частей пути
  lastRecipientCallSign: ''

  routePrefix: ''
  defaultRoute: ''

  routes:
    '*route/': 'onTrailingSlash'
    '*route': 'onRoute'

  initialize: ->
    window.router = @

    setTimeout( ->
      if Backbone.history
        Backbone.history.start()
    0)

  onRoute: (route) ->
    @currentRoute = route

    @lastRecipientCallSign = ''
    routeFragments = if route then @clearRoute(route).split('/') else []
    routeFragments.unshift('show')
    for index in [0...routeFragments.length]
      callSign = routeFragments.slice(0, index + 1).join('/')
      routeTail = routeFragments.slice(index + 1)
      @trigger(callSign, routeTail)

    if callSign != @lastRecipientCallSign
      setTimeout(@correctRoute, 0)

  # при неправильном пути перенаправлять на последний путь, от которого был получен ответ, или на путь по-умолчанию
  correctRoute: =>
    lastRegisteredRoute = @lastRecipientCallSign.split('/').slice(1).join('/')
    if lastRegisteredRoute
      @currentRoute = lastRegisteredRoute
      @navigateReplacingSilent(lastRegisteredRoute)
    else
      @navigateReplacing(@defaultRoute)

  # убрать завершающий слэш
  onTrailingSlash: (route) ->
    if @routePrefix.indexOf(route) == 0
      @onRoute()
    else
      @navigateReplacing(@clearRoute(route))

  navigateReplacing: (route) ->
    Backbone.history.navigate("#{@routePrefix}#{route}", trigger: true, replace: true)

  navigateReplacingSilent: (route) ->
    Backbone.history.navigate("#{@routePrefix}#{route}", trigger: false, replace: true)

  acknowledgeReceipt: (obj) ->
    @lastRecipientCallSign = obj.callSign

  clearRoute: (route) ->
    route.replace(@routePrefix, '')