(function() {
  var _ref, _ref1, _ref10, _ref11, _ref2, _ref3, _ref4, _ref5, _ref6, _ref7, _ref8, _ref9,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; },
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.Extensions = {
    View: {},
    utils: {
      keyCode: {
        BACKSPACE: 8,
        COMMA: 188,
        DELETE: 46,
        DOWN: 40,
        END: 35,
        ENTER: 13,
        ESCAPE: 27,
        HOME: 36,
        LEFT: 37,
        NUMPAD_ADD: 107,
        NUMPAD_DECIMAL: 110,
        NUMPAD_DIVIDE: 111,
        NUMPAD_ENTER: 108,
        NUMPAD_MULTIPLY: 106,
        NUMPAD_SUBTRACT: 109,
        PAGE_DOWN: 34,
        PAGE_UP: 33,
        PERIOD: 190,
        RIGHT: 39,
        SPACE: 32,
        TAB: 9,
        UP: 38
      },
      nestedToJSON: function(data) {
        var key, value;
        for (key in data) {
          value = data[key];
          if (value instanceof Extensions.Model || value instanceof Extensions.Collection) {
            data[key] = value.toJSON();
          }
        }
        return data;
      },
      readImage: function(file, callback, method) {
        var fileReader;
        if (window.FileReader) {
          fileReader = new FileReader();
          fileReader.onload = fileReader.onerror = callback;
          method = method || 'readAsDataURL';
          if (fileReader[method]) {
            fileReader[method](file);
            return fileReader;
          }
        }
        return false;
      },
      flattenTree: function(tree, options) {
        var flatten, flattenedTree;
        if (options == null) {
          options = {};
        }
        flattenedTree = {};
        _.defaults(options, {
          glue: ':',
          shouldStop: function(key, branch) {
            return !_.isObject(branch) || _.isArray(branch) || _.isFunction(branch);
          },
          getContent: function(key, branch, path) {
            return branch;
          }
        });
        if (_.isObject(tree)) {
          (flatten = function(tree, path) {
            var branch, key, _results;
            _results = [];
            for (key in tree) {
              branch = tree[key];
              _results.push((function(key, branch, path) {
                path.push(key);
                if (options.shouldStop(key, branch)) {
                  return flattenedTree[path.join(options.glue)] = options.getContent(key, branch, path);
                } else {
                  return flatten(branch, path);
                }
              })(key, branch, path.slice(0)));
            }
            return _results;
          })(tree, []);
        } else {
          flattenedTree[tree] = options.getContent(tree);
        }
        return flattenedTree;
      },
      extensionOf: function(baseConstructor, extendedConstructor) {
        var _ref, _ref1;
        if (!extendedConstructor) {
          return false;
        }
        if (((_ref = extendedConstructor.__super__) != null ? _ref.constructor : void 0) === baseConstructor) {
          return true;
        }
        return Extensions.utils.extensionOf(baseConstructor, (_ref1 = extendedConstructor.__super__) != null ? _ref1.constructor : void 0);
      }
    }
  };

  /*
    прототип модели
  */


  Extensions.Model = (function(_super) {
    __extends(Model, _super);

    function Model() {
      _ref = Model.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    Model.prototype.toJSON = function(options) {
      var data, _ref1, _ref2, _ref3;
      data = Extensions.utils.nestedToJSON(Model.__super__.toJSON.call(this));
      if (this === ((_ref1 = this.collection) != null ? _ref1.newModel : void 0) || (this.originalModel && this.originalModel === ((_ref2 = this.originalModel) != null ? (_ref3 = _ref2.collection) != null ? _ref3.newModel : void 0 : void 0))) {
        data.id = 'new';
      }
      return data;
    };

    Model.prototype.set = function(key, val, options) {
      var attr, attributes, value, _fn, _ref1,
        _this = this;
      if (typeof key === 'object') {
        attributes = key;
        options = val;
      } else {
        attributes = {};
        attributes[key] = val;
      }
      attributes = this.restoreByKeys(attributes);
      Model.__super__.set.call(this, attributes, options);
      _ref1 = this.changed;
      _fn = function(attr, value) {
        if (value instanceof Extensions.Model || value instanceof Extensions.Collection) {
          return _this.listenTo(value, 'deep:change', function() {
            _this.trigger("change:" + attr);
            return _this.trigger('change');
          });
        }
      };
      for (attr in _ref1) {
        value = _ref1[attr];
        _fn(attr, value);
      }
      return this;
    };

    Model.prototype.unset = function(attr) {
      var value;
      value = this.get(attr);
      if (value instanceof Extensions.Model || value instanceof Extensions.Collection) {
        this.stopListening(value, 'deep:change');
      }
      return Model.__super__.unset.apply(this, arguments);
    };

    Model.prototype.restoreByKeys = function(attributes) {
      var attr, data, id, key, source, sourceObj, _ref1;
      _ref1 = this.keys;
      for (attr in _ref1) {
        source = _ref1[attr];
        for (key in source) {
          sourceObj = source[key];
          if (id = attributes[key]) {
            if (data = sourceObj().get(attributes[key])) {
              attributes[attr] = data;
              delete attributes[key];
            }
          }
        }
      }
      return attributes;
    };

    Model.prototype.save = function(options) {
      var newModel;
      if (this.id === 'new') {
        newModel = this.clone();
        newModel.save(options);
        this.collection.add(newModel.set('id', _.uniqueId('')));
        return this.set(new this.constructor().toJSON());
      } else {
        return Model.__super__.save.call(this, this.attributes, options);
      }
    };

    Model.prototype.trigger = function(name) {
      Model.__super__.trigger.apply(this, arguments);
      if (name === 'change') {
        return this.trigger('deep:change');
      }
    };

    Model.prototype.isEmpty = function() {
      var attribute, defaults, isDefaultValueChanged, isNewPropAdded, isObjectEmpty, value, _ref1;
      if (_.isEmpty(this.attributes)) {
        return true;
      }
      defaults = _.result(this, 'defaults');
      _ref1 = this.attributes;
      for (attribute in _ref1) {
        value = _ref1[attribute];
        isNewPropAdded = function() {
          var _ref2;
          return _ref2 = !attribute, __indexOf.call(defaults, _ref2) >= 0;
        };
        isDefaultValueChanged = function() {
          return !_.isObject(value) && defaults[attribute] !== value;
        };
        isObjectEmpty = function() {
          return _.isObject(value) && value.isEmpty && !value.isEmpty();
        };
        if (isNewPropAdded() || isDefaultValueChanged() || isObjectEmpty()) {
          return false;
        }
      }
      return true;
    };

    return Model;

  })(Backbone.Model);

  /*
    прототип коллекции
  */


  Extensions.Collection = (function(_super) {
    __extends(Collection, _super);

    function Collection() {
      _ref1 = Collection.__super__.constructor.apply(this, arguments);
      return _ref1;
    }

    Collection.prototype.model = Extensions.Model;

    Collection.prototype.initialize = function() {
      Collection.__super__.initialize.apply(this, arguments);
      return this.createNewModel();
    };

    Collection.prototype.toJSON = function() {
      return Extensions.utils.nestedToJSON(Collection.__super__.toJSON.call(this));
    };

    Collection.prototype.createNewModel = function() {
      var _this = this;
      this.newModel = new this.model;
      this.newModel.collection = this;
      this.newModel.id = 'new';
      return this.listenTo(this.newModel, 'change', function() {
        return _this.trigger('change:newModel');
      });
    };

    Collection.prototype.trigger = function(name) {
      Collection.__super__.trigger.apply(this, arguments);
      if (name.indexOf('change') === 0) {
        return this.trigger('deep:change', name.split(':').slice(1).join(':'));
      }
    };

    return Collection;

  })(Backbone.Collection);

  /*
    прототип набора разнотипных данных
  
    new Extensions.Set(
      ship: shipModel
      planes: planesCollection
    )
  */


  Extensions.Set = (function(_super) {
    __extends(Set, _super);

    function Set(data, options) {
      var dataKey, dataPiece, models;
      models = [];
      if (data) {
        for (dataKey in data) {
          dataPiece = data[dataKey];
          models.push(new Extensions.Model({
            id: dataKey,
            data: dataPiece
          }));
        }
      }
      Set.__super__.constructor.call(this, models, options);
    }

    return Set;

  })(Extensions.Collection);

  /*
    прототип отображения для работы с рутером
  */


  Extensions.View.RouterBridge = (function(_super) {
    __extends(RouterBridge, _super);

    function RouterBridge() {
      this.onRouterNotification = __bind(this.onRouterNotification, this);
      _ref2 = RouterBridge.__super__.constructor.apply(this, arguments);
      return _ref2;
    }

    RouterBridge.extensionOf = function(constructor) {
      return Extensions.utils.extensionOf(constructor, this);
    };

    RouterBridge.prototype.callSign = false;

    RouterBridge.prototype.initialize = function(options) {
      RouterBridge.__super__.initialize.apply(this, arguments);
      this.options = options || {};
      return this.listenToRouter();
    };

    RouterBridge.prototype.listenToRouter = function() {
      if (this.options.callSign) {
        this.callSign = this.options.callSign;
      }
      if (this.callSign) {
        return this.listenTo(router, this.callSign, this.onRouterNotification);
      }
    };

    RouterBridge.prototype.onRouterNotification = function() {
      return router.acknowledgeReceipt(this);
    };

    return RouterBridge;

  })(Backbone.View);

  /*
    прототип отображения для работы с DOM
  */


  Extensions.View.DomBridge = (function(_super) {
    __extends(DomBridge, _super);

    function DomBridge() {
      this.onRouterNotification = __bind(this.onRouterNotification, this);
      _ref3 = DomBridge.__super__.constructor.apply(this, arguments);
      return _ref3;
    }

    DomBridge.prototype.removeTimeout = 0.2 * 60 * 1000;

    DomBridge.prototype.initialize = function() {
      DomBridge.__super__.initialize.apply(this, arguments);
      if (this.options.template) {
        this.template = this.options.template;
      }
      if (!this.options.dontRenderOnInit) {
        return this.render();
      }
    };

    DomBridge.prototype.keepPlaceholder = function() {
      if (this.options.el) {
        return this.$placeholderEl || (this.$placeholderEl = this.$el);
      }
    };

    DomBridge.prototype.restorePlaceholder = function() {
      if (this.$placeholderEl) {
        return this.$el.after(this.$placeholderEl);
      }
    };

    DomBridge.prototype.render = function() {
      this.renderHTML();
      this.onRender();
      return this;
    };

    DomBridge.prototype.onRender = function() {
      return this.selectElements();
    };

    DomBridge.prototype.serializeData = function() {
      var _ref4;
      return _.extend(this.getPath(), this.getState(), (_ref4 = this.model) != null ? _ref4.toJSON() : void 0);
    };

    DomBridge.prototype.getState = function() {
      return {};
    };

    DomBridge.prototype.getPath = function() {
      var path;
      if (this.callSign) {
        path = '#' + router.routePrefix + this.callSign.slice('show/'.length);
        if (path[path.length - 1] === '/') {
          path = path.slice(0, path.length - 1);
        }
      }
      return {
        path: path,
        currentPath: '#' + router.currentRoute
      };
    };

    DomBridge.prototype.renderHTML = function() {
      var outerEl;
      if (this.template) {
        this.keepPlaceholder();
        outerEl = $(this.template(this.serializeData()));
        this.$el.replaceWith(outerEl);
        return this.setElement(outerEl);
      }
    };

    DomBridge.prototype.selectElements = function() {
      var key, selector, value, _ref4, _ref5, _results;
      if (this.elements) {
        this._elements_selectors || (this._elements_selectors = this.elements);
        this._additionalPartialData = {};
        this.elements = {};
        _ref4 = this._elements_selectors;
        _results = [];
        for (key in _ref4) {
          value = _ref4[key];
          selector = (_ref5 = value.el) != null ? _ref5 : value;
          this.selectElement(key, selector);
          _results.push(this._additionalPartialData[key] = value.data || function() {});
        }
        return _results;
      }
    };

    DomBridge.prototype.selectElement = function(key, selector) {
      return this.elements[key] = this.$(selector);
    };

    DomBridge.prototype.onRouterNotification = function(routeTail) {
      DomBridge.__super__.onRouterNotification.apply(this, arguments);
      return this.show(routeTail);
    };

    DomBridge.prototype.show = function(routeTail) {
      this.onShow();
      return this.$el.show();
    };

    DomBridge.prototype.onShow = function() {
      if (this.removeSchedule) {
        clearTimeout(this.removeSchedule);
        return this.removeSchedule = null;
      }
    };

    DomBridge.prototype.hide = function() {
      this.onHide();
      return this.$el.hide();
    };

    DomBridge.prototype.onHide = function() {
      var _this = this;
      if (this.removeTimeout) {
        return this.removeSchedule = setTimeout(function() {
          return _this.remove();
        }, this.removeTimeout);
      }
    };

    DomBridge.prototype.remove = function() {
      this.onRemove();
      return DomBridge.__super__.remove.apply(this, arguments);
    };

    DomBridge.prototype.onRemove = function() {
      if (this.removeSchedule) {
        clearTimeout(this.removeSchedule);
      }
      this.trigger('removing');
      return this.restorePlaceholder();
    };

    return DomBridge;

  })(Extensions.View.RouterBridge);

  /*
    прототип отображения-контейнера
  */


  Extensions.Container = (function(_super) {
    __extends(Container, _super);

    function Container() {
      _ref4 = Container.__super__.constructor.apply(this, arguments);
      return _ref4;
    }

    Container.prototype.viewKey = function(model) {
      return model.id;
    };

    Container.prototype.initialize = function() {
      Container.__super__.initialize.apply(this, arguments);
      if (this.options.viewKey) {
        this.viewKey = this.options.viewKey;
      }
      if (this.options.view) {
        this.view = this.options.view;
      }
      this.view || (this.view = {});
      return this.views = {};
    };

    Container.prototype.show = function(routeTail) {
      return this.$el.show();
    };

    Container.prototype.hide = function() {
      return this.$el.hide();
    };

    Container.prototype.ensureView = function(key) {
      var _this = this;
      if (!this.views[key]) {
        this.views[key] = this.createView(key);
        if (this.views[key]) {
          if (!this.$el.has(this.views[key].$el).length) {
            this.$el.append(this.views[key].$el);
          }
          return this.listenTo(this.views[key], 'removing', function() {
            return _this.clear(key);
          });
        }
      }
    };

    Container.prototype.createView = function(key) {
      var model, view, _ref5;
      model = this.collection.get(key);
      if (model) {
        view = this.view[this.viewKey(model)];
      }
      if (view instanceof Function) {
        view = view.call(this, model);
      }
      if (view != null ? (_ref5 = view[0]) != null ? _ref5.extensionOf(Extensions.View.DomBridge) : void 0 : void 0) {
        return new view[0](_.extend({
          model: model,
          callSign: this instanceof Extensions.Commutator ? "" + this.callSign + "/" + key : this.callSign,
          parentView: this
        }, view[1]));
      }
    };

    Container.prototype.showView = function(key, routeTail) {
      this.ensureView(key);
      if (this.views[key]) {
        return this.views[key].show(routeTail);
      }
    };

    Container.prototype.hideView = function(key) {
      if (this.views[key]) {
        return this.views[key].hide();
      }
    };

    Container.prototype.clear = function(key) {
      this.stopListening(this.views[key]);
      return this.views[key] = null;
    };

    Container.prototype.onRemove = function() {
      var view;
      for (view in this.views) {
        if (view != null) {
          view.remove();
        }
      }
      return Container.__super__.onRemove.apply(this, arguments);
    };

    return Container;

  })(Extensions.View.DomBridge);

  /*
    прототип коммутатора и селектора отображений
  */


  Extensions.Commutator = (function(_super) {
    __extends(Commutator, _super);

    function Commutator() {
      _ref5 = Commutator.__super__.constructor.apply(this, arguments);
      return _ref5;
    }

    Commutator.prototype.show = function(routeTail) {
      var key;
      Commutator.__super__.show.apply(this, arguments);
      key = routeTail[0];
      if (this.exists(key)) {
        if (!this.isCurrent(key)) {
          this.hideView(this.currentKey);
          this.currentKey = key;
        }
        return this.showView(key, routeTail.slice(1));
      } else {
        return this.hideView(this.currentKey);
      }
    };

    Commutator.prototype.hide = function() {
      this.hideView(this.currentKey);
      this.currentKey = null;
      return Commutator.__super__.hide.apply(this, arguments);
    };

    Commutator.prototype.exists = function(key) {
      return !!this.collection.get(key);
    };

    Commutator.prototype.clear = function(key) {
      this.stopListening(this.views[key]);
      if (this.isCurrent(key)) {
        this._currentView = null;
      }
      return this.views[key] = null;
    };

    Commutator.prototype.isCurrent = function(key) {
      return this.currentKey && this.currentKey === key;
    };

    return Commutator;

  })(Extensions.Container);

  /*
    прототип списка из элементов коллекции
  */


  Extensions.List = (function(_super) {
    __extends(List, _super);

    function List() {
      this.onSelect = __bind(this.onSelect, this);
      _ref6 = List.__super__.constructor.apply(this, arguments);
      return _ref6;
    }

    List.prototype.maxSelected = 0;

    List.prototype.initialize = function() {
      var _this = this;
      List.__super__.initialize.apply(this, arguments);
      if (this.collection) {
        this.listenTo(this.collection, 'add', function(model) {
          return _this.showView(model.id);
        });
      }
      return this.initSelections();
    };

    List.prototype.show = function(routeTail) {
      var _this = this;
      List.__super__.show.apply(this, arguments);
      if (this.collection) {
        return this.collection.each(function(model) {
          return _this.showView(model.id, routeTail);
        });
      }
    };

    List.prototype.hide = function() {
      var key;
      for (key in this.views) {
        this.hideView(key);
      }
      return List.__super__.hide.apply(this, arguments);
    };

    List.prototype.clear = function(key) {
      this.stopListening(this.views[key]);
      return this.views[key] = null;
    };

    List.prototype.initSelections = function() {
      if (this.maxSelected && this.collection) {
        this.selectedItems = new Extensions.Collection;
        return this.collection.on('select', this.onSelect);
      }
    };

    List.prototype.onSelect = function(model) {
      var removingModel;
      if (this.selectedItems.get(model.id)) {
        this.selectedItems.remove(model);
        return model.trigger('unselected');
      } else {
        this.selectedItems.add(model);
        if (this.selectedItems.length > this.maxSelected) {
          removingModel = this.selectedItems.first();
          this.selectedItems.remove(removingModel);
          removingModel.trigger('unselected');
        }
        return model.trigger('selected');
      }
    };

    return List;

  })(Extensions.Container);

  /*
    прототип отображения модели
  */


  Extensions.View.Item = (function(_super) {
    __extends(Item, _super);

    function Item() {
      this.partiallyRenderRoot = __bind(this.partiallyRenderRoot, this);
      this.partiallyRender = __bind(this.partiallyRender, this);
      _ref7 = Item.__super__.constructor.apply(this, arguments);
      return _ref7;
    }

    Item.prototype.initialize = function() {
      Item.__super__.initialize.apply(this, arguments);
      return this.initModelEvents();
    };

    Item.prototype.getState = function() {
      return _.extend(Item.__super__.getState.apply(this, arguments), this.getSelectionState());
    };

    Item.prototype.getSelectionState = function() {
      var _ref10, _ref8, _ref9;
      return {
        selected: !!((_ref8 = this.options.parentView) != null ? (_ref9 = _ref8.selectedItems) != null ? _ref9.get((_ref10 = this.model) != null ? _ref10.id : void 0) : void 0 : void 0)
      };
    };

    Item.prototype.initModelEvents = function() {
      var event, handlers, _ref8, _results,
        _this = this;
      if (this.model) {
        this.modelEvents || (this.modelEvents = {});
        this.modelEvents = Extensions.utils.flattenTree(this.modelEvents);
        this.initAutoRender();
        _ref8 = this.modelEvents;
        _results = [];
        for (event in _ref8) {
          handlers = _ref8[event];
          _results.push((function(event, handlers) {
            var handler, _i, _len, _results1;
            if (_.isString(handlers) || _.isFunction(handlers)) {
              handlers = [handlers];
            }
            _results1 = [];
            for (_i = 0, _len = handlers.length; _i < _len; _i++) {
              handler = handlers[_i];
              _results1.push((function(handler) {
                if (_.isString(handler) && _.isFunction(_this[handler])) {
                  handler = _this[handler];
                }
                if (_.isFunction(handler)) {
                  return _this.listenTo(_this.model, event.replace(':_', ''), handler);
                }
              })(handler));
            }
            return _results1;
          })(event, handlers));
        }
        return _results;
      }
    };

    Item.prototype.initAutoRender = function() {
      var eventsTree, renderType, _ref8, _results,
        _this = this;
      if (this.autoRender) {
        if (_.isString(this.autoRender) || _.isArray(this.autoRender)) {
          this.autoRender = {
            _all: this.autoRender
          };
        }
        _ref8 = this.autoRender;
        _results = [];
        for (renderType in _ref8) {
          eventsTree = _ref8[renderType];
          _results.push((function(renderType, eventsTree) {
            var event, events, getRenderMethod, handler, _results1;
            getRenderMethod = function() {
              switch (renderType) {
                case '_all':
                  return _this.render;
                case '_root':
                  return _this.partiallyRenderRoot;
                default:
                  return function(key, branch, path) {
                    return _this.partiallyRender(renderType);
                  };
              }
            };
            events = Extensions.utils.flattenTree(eventsTree, {
              getContent: getRenderMethod
            });
            _results1 = [];
            for (event in events) {
              handler = events[event];
              if (!_this.modelEvents[event]) {
                _this.modelEvents[event] = [];
              }
              if (!_.isArray(_this.modelEvents[event])) {
                _this.modelEvents[event] = [_this.modelEvents[event]];
              }
              _results1.push(_this.modelEvents[event].push(handler));
            }
            return _results1;
          })(renderType, eventsTree));
        }
        return _results;
      }
    };

    Item.prototype.partiallyRender = function(elementKey) {
      var data, partial_el;
      data = this.serializeData();
      data['_render_partial'] = elementKey;
      partial_el = $(this.template(data));
      this.elements[elementKey].replaceWith(partial_el);
      return this.elements[elementKey] = partial_el;
    };

    Item.prototype.partiallyRenderRoot = function() {
      var data;
      data = this.serializeData();
      data['_render_root'] = true;
      return this.updateRootAttributes($(this.template(data)));
    };

    Item.prototype.updateRootAttributes = function(outerEl) {
      var attribute, _i, _len, _ref8, _results;
      _ref8 = outerEl.prop('attributes');
      _results = [];
      for (_i = 0, _len = _ref8.length; _i < _len; _i++) {
        attribute = _ref8[_i];
        _results.push(this.$el.attr(attribute.name, attribute.value));
      }
      return _results;
    };

    return Item;

  })(Extensions.View.DomBridge);

  /*
    прототип формы для редактирования данных модели
  */


  Extensions.View.Form = (function(_super) {
    __extends(Form, _super);

    function Form() {
      this.cancelForm = __bind(this.cancelForm, this);
      this.saveForm = __bind(this.saveForm, this);
      _ref8 = Form.__super__.constructor.apply(this, arguments);
      return _ref8;
    }

    Form.prototype.initialize = function() {
      var _ref9;
      this.originalModel = this.model;
      this.model = this.originalModel.clone();
      this.model.callSign = (_ref9 = this.options) != null ? _ref9.callSign.replace('/edit', '') : void 0;
      return Form.__super__.initialize.apply(this, arguments);
    };

    Form.prototype.onRender = function() {
      Form.__super__.onRender.apply(this, arguments);
      return this.bindValues();
    };

    Form.prototype.getState = function() {
      return _.extend(Form.__super__.getState.apply(this, arguments), this.getFormState());
    };

    Form.prototype.getFormState = function() {
      var data, hasChanged, isVisible;
      isVisible = !!this.isFormVisible;
      hasChanged = !_.isEqual(this.originalModel.attributes, this.model.attributes);
      data = {};
      data['cancelable'] = isVisible;
      data['savable'] = hasChanged && isVisible;
      data['continuable'] = hasChanged && !isVisible;
      return data;
    };

    Form.prototype.bindValues = function() {
      var attrName, value, _ref9, _results;
      if (this.values) {
        _ref9 = this.values;
        _results = [];
        for (attrName in _ref9) {
          value = _ref9[attrName];
          _results.push(this.bindValue(attrName, value));
        }
        return _results;
      }
    };

    Form.prototype.bindValue = function(attrName, value) {
      var el, events, process, selector, _ref10, _ref11, _ref9,
        _this = this;
      selector = (_ref9 = value.el) != null ? _ref9 : value;
      el = this.$(selector);
      events = (_ref10 = value.events) != null ? _ref10 : 'change keydown';
      process = (_ref11 = value.process) != null ? _ref11 : function(val) {
        return val;
      };
      if (el.is('input[type=text], textarea')) {
        this.$el.on(events, selector, function(e) {
          var oldValue;
          if (e.type === 'keydown') {
            switch (e.keyCode) {
              case Extensions.utils.keyCode.ESCAPE:
                oldValue = _this.originalModel.get(attrName);
                _this.model.set(attrName, oldValue);
                return el.val(oldValue);
              default:
                return setTimeout(function() {
                  return _this.model.set(attrName, process(el.val()));
                }, 50);
            }
          }
        });
      }
      if (el.is('input[type=file]')) {
        return el.fileupload({
          url: 'api/brand/logo',
          dataType: 'json',
          add: function(e, data) {
            Extensions.utils.readImage(data.files[0], function(fileReader) {
              if (fileReader) {
                return _this.model.set(attrName, fileReader.target.result);
              }
            });
            return data.submit();
          },
          done: function(e, data) {
            return _this.model.set(attrName, data.result.files[0].url);
          }
        });
      }
    };

    Form.prototype.saveForm = function(onSuccess) {
      this.saveModel(onSuccess);
      return this.remove();
    };

    Form.prototype.cancelForm = function() {
      return this.remove();
    };

    Form.prototype.onShow = function() {
      Form.__super__.onShow.apply(this, arguments);
      this.isFormVisible = true;
      return this.model.trigger("change");
    };

    Form.prototype.onHide = function() {
      Form.__super__.onHide.apply(this, arguments);
      this.isFormVisible = false;
      return this.model.trigger("change");
    };

    Form.prototype.onRemove = function() {
      Form.__super__.onRemove.apply(this, arguments);
      return this.trigger('delete');
    };

    Form.prototype.saveModel = function(onSuccess) {
      this.originalModel.set(this.model.attributes);
      this.originalModel.save({
        success: onSuccess
      });
      return this.originalModel.trigger('change');
    };

    return Form;

  })(Extensions.View.Item);

  /*
    прототип дерева отображений
  */


  Extensions.Tree = (function(_super) {
    __extends(Tree, _super);

    function Tree() {
      _ref9 = Tree.__super__.constructor.apply(this, arguments);
      return _ref9;
    }

    Tree.prototype.viewKey = function(model) {
      return 'item';
    };

    Tree.nodesPropertyName = 'nodes';

    Tree.prototype.itemView = function(model) {
      return Extensions.View.Item;
    };

    Tree.prototype.nodesView = function(model) {
      return Extensions.Tree;
    };

    Tree.prototype.view = {
      item: function(model) {
        debugger;
        var _this = this;
        return [
          Extensions.List, {
            collection: new Extensions.Set({
              item: _.extend(model.get('data') || new Extensions.Model, {
                id: model.id
              }),
              nodes: _.extend(model.get('data').get('nodes') || new Extensions.Collection, {
                id: model.id
              })
            }),
            view: {
              item: function(model) {
                debugger;
                return [
                  _this.itemView(), {
                    callSign: "" + _this.callSign + "/" + (model.get('data').id),
                    model: model.get('data') || new Extensions.Model
                  }
                ];
              },
              nodes: function(model) {
                debugger;
                return [
                  Extensions.Commutator, {
                    callSign: "" + _this.callSign + "/" + (model.get('data').id),
                    collection: model.get('data') || new Extensions.Collection,
                    viewKey: function(model) {
                      return 'item';
                    },
                    view: {
                      item: function(model) {
                        debugger;
                        return [
                          Extensions.Tree, {
                            callSign: "" + _this.callSign + "/" + (model.get('data').id),
                            collection: model.get('data').get('nodes') || new Extensions.Collection
                          }
                        ];
                      }
                    }
                  }
                ];
              }
            }
          }
        ];
      }
    };

    return Tree;

  })(Extensions.List);

  /*
    прототип рутера
  */


  Extensions.Router = (function(_super) {
    __extends(Router, _super);

    function Router() {
      this.correctRoute = __bind(this.correctRoute, this);
      _ref10 = Router.__super__.constructor.apply(this, arguments);
      return _ref10;
    }

    Router.prototype.lastRecipientCallSign = '';

    Router.prototype.routePrefix = '';

    Router.prototype.defaultRoute = '';

    Router.prototype.routes = {
      '*route/': 'onTrailingSlash',
      '*route': 'onRoute'
    };

    Router.prototype.initialize = function() {
      window.router = this;
      return setTimeout(function() {
        if (Backbone.history) {
          return Backbone.history.start();
        }
      }, 0);
    };

    Router.prototype.onRoute = function(route) {
      var callSign, index, routeFragments, routeTail, _i, _ref11;
      this.currentRoute = route;
      this.lastRecipientCallSign = '';
      routeFragments = route ? this.clearRoute(route).split('/') : [];
      routeFragments.unshift('show');
      for (index = _i = 0, _ref11 = routeFragments.length; 0 <= _ref11 ? _i < _ref11 : _i > _ref11; index = 0 <= _ref11 ? ++_i : --_i) {
        callSign = routeFragments.slice(0, index + 1).join('/');
        routeTail = routeFragments.slice(index + 1);
        this.trigger(callSign, routeTail);
      }
      if (callSign !== this.lastRecipientCallSign) {
        return setTimeout(this.correctRoute, 0);
      }
    };

    Router.prototype.correctRoute = function() {
      var lastRegisteredRoute;
      lastRegisteredRoute = this.lastRecipientCallSign.split('/').slice(1).join('/');
      if (lastRegisteredRoute) {
        this.currentRoute = lastRegisteredRoute;
        return this.navigateReplacingSilent(lastRegisteredRoute);
      } else {
        return this.navigateReplacing(this.defaultRoute);
      }
    };

    Router.prototype.onTrailingSlash = function(route) {
      if (this.routePrefix.indexOf(route) === 0) {
        return this.onRoute();
      } else {
        return this.navigateReplacing(this.clearRoute(route));
      }
    };

    Router.prototype.navigateReplacing = function(route) {
      return Backbone.history.navigate("" + this.routePrefix + route, {
        trigger: true,
        replace: true
      });
    };

    Router.prototype.navigateReplacingSilent = function(route) {
      return Backbone.history.navigate("" + this.routePrefix + route, {
        trigger: false,
        replace: true
      });
    };

    Router.prototype.acknowledgeReceipt = function(obj) {
      return this.lastRecipientCallSign = obj.callSign;
    };

    Router.prototype.clearRoute = function(route) {
      return route.replace(this.routePrefix, '');
    };

    return Router;

  })(Backbone.Router);

  /*
    прототип приложения
  */


  Extensions.Application = (function(_super) {
    __extends(Application, _super);

    function Application() {
      _ref11 = Application.__super__.constructor.apply(this, arguments);
      return _ref11;
    }

    Application.prototype.initialize = function() {
      return window.app = this;
    };

    return Application;

  })(Backbone.View);

  _.extend(Extensions.Application, Backbone.Events);

}).call(this);
